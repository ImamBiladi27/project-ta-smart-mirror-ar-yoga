import 'dart:async';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_mlkit_pose_detection/google_mlkit_pose_detection.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:simple_knn/simple_knn.dart';
import '../main.dart';
import '../model/padahastana.dart';
import '../model/virabhadrasana2.dart';
import '../providers/pose4_provider.dart';
import '../providers/pose_provider.dart';

import 'camera_view.dart';
import 'painters/pose_painter.dart';
import 'package:typed_data/typed_data.dart';
import 'package:data/data.dart';
import 'package:google_ml_kit_example/model/padahastana.dart';

import 'second_route.dart';

class PoseDetectorView3 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PoseDetectorViewState();
}

class _PoseDetectorViewState extends State<PoseDetectorView3> {
  PoseDetector _poseDetector = PoseDetector(options: PoseDetectorOptions());
  //final audioPlayer = AudioPlayer();
  bool isPlaying = false;
  Duration duration = Duration.zero;

  bool _isBusy = false;
  CustomPaint? customPaint;
  String? _text;
  String a = 'salah';
  //get b => a;
  final player = AudioPlayer();

  final player2 = AudioPlayer();
  var statusBenar = null;
  var statusBenar2 = null;
  var statusBenar3 = null;
  var statusBenar4 = null;
  var statusBenar5 = null;
  var statusBenar6 = null;

  @override
  void dispose() async {
    //_canProcess = false;

    super.dispose();
    await _poseDetector.close();
  }

  void updateState(String type) {
    // setState(() {
    a = type;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Stack(
          children: <Widget>[
            CameraView(
              title: 'Virabhadrasana II',
              image: Image.asset('assets/VirabhadrasanaII.jpg'),
              customPaint: customPaint,
              onImage: (inputImage) {
                //_startTimer();
                processImage(inputImage, context);
                a = '';
                // processImage2(inputImage);
              },
            ),
            Positioned(
                top: 90,
                right: 1,
                child: Container(
                  width: 100,
                  height: 100,
                  child:
                      Card(child: Image.asset('assets/VirabhadrasanaII.jpg')),
                )),
            FutureBuilder<String>(
              //future: processImage(param1),
              builder: (BuildContext context, AsyncSnapshot snapshot) =>
                  Positioned(
                bottom: 170,
                left: 100,
                //leading:Image.asset('name'),
                child: LinearPercentIndicator(
                  width: 250,
                  lineHeight: 30,
                  percent: 0.0,
                  center: Text(
                    //  "${(double.parse(percent))}",
                    '${a}',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
                  ),
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  progressColor: Colors.greenAccent,
                  backgroundColor: Colors.white,
                  animation: true,
                  animationDuration: 5000,
                ),
              ),
            )
          ],
        ),
        builder: EasyLoading.init());
  }

  Future<void> processImage(
    InputImage inputImage,
    context,
  ) async {
    Pose4Provider provider = new Pose4Provider();
    String b2 = "";
    //  player.play(UrlSource('note1.wave'));

// If file located in assets folder like assets/sounds/note01.wave"

    //await player.play(AssetSource('assets/dummy/countdown_flutter.mp3'));
    if (_isBusy) return;
    _isBusy = true;
    var _arrLandmark = [];
    List<Virabhadrasana2> getVirabhadrasana2 =
        await Pose4Provider.getVirabhadrasana2();

    // List<Padahastana> getPadahastana11 = await PoseProvider.getPadahastana11();
    // List<Padahastana> getPadahastana12 = await PoseProvider.getPadahastana12();
    // List<Padahastana> getPadahastana13 = await PoseProvider.getPadahastana13();
    // List<Padahastana> getPadahastana14 = await PoseProvider.getPadahastana14();

    List<PoseLandmark> listPL = [];
    List<Virabhadrasana2> listpada = [];
    List<PoseLandmark> listLandmark = [];
    List<String> listType = [];
    var batas = 100;
    final data_poses = await _poseDetector.processImage(inputImage);

    for (Pose pose in data_poses) {
      pose.landmarks.forEach((_, landmark) {
        List _landmarkXY = [];
        String type = landmark.type.toString();

        //String type2 = landmark.type.toString();
        final x = landmark.x;
        final y = landmark.y;

        print('sinkron getVirabhadrasana2');
        for (var i = 0; i < getVirabhadrasana2.length; i++) {
          Virabhadrasana2 sinkron = getVirabhadrasana2[i];

          if (type == sinkron.key1 ||
              type == sinkron.key2 ||
              type == sinkron.key3 ||
              type == sinkron.key4) {
            listLandmark.add(landmark);
            print('masuk1');
          }
        }
      });
      final landmark = pose.landmarks[PoseLandmarkType.nose];
    }

    print('list $listLandmark');
    //print('ini listType $listType');

    var statusBenar = null;
    var statusBenar2 = null;
    var statusBenar3 = null;
    var statusBenar4 = null;
    var statusBenar5 = null;
    var statusBenar6 = null;
    for (final e1 in listLandmark) {
      for (Virabhadrasana2 perbandingan in getVirabhadrasana2) {
        PoseLandmark poseLandmark1 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key1)
            .toList()[0];

        PoseLandmark poseLandmark2 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key2)
            .toList()[0];

        PoseLandmark poseLandmark3 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key3)
            .toList()[0];
        PoseLandmark poseLandmark4 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key4)
            .toList()[0];

        double sisi1;
        double sisi2;
        double sisi3;
        double sisi4;
        if (poseLandmark1.x > poseLandmark2.x) {
          sisi1 = poseLandmark1.x - poseLandmark2.x;
        } else {
          sisi1 = poseLandmark2.x - poseLandmark1.x;
        }
        if (poseLandmark3.x > poseLandmark4.x) {
          sisi3 = poseLandmark3.x - poseLandmark4.x;
        } else {
          sisi3 = poseLandmark4.x - poseLandmark3.x;
        }
        if (poseLandmark1.y > poseLandmark2.y) {
          sisi2 = poseLandmark1.y - poseLandmark2.y;
        } else {
          sisi2 = poseLandmark2.y - poseLandmark1.y;
        }
        if (poseLandmark3.y > poseLandmark4.y) {
          sisi4 = poseLandmark3.y - poseLandmark4.y;
        } else {
          sisi4 = poseLandmark4.y - poseLandmark3.y;
        }

        ///pytagoras
        double jarakReal = sqrt((sisi1 * sisi1) + (sisi2 * sisi2));

        //normalisasi
        //double jarakNormalisasi = jarakReal/100;
        double jarakReal2 = sqrt((sisi3 * sisi3) + (sisi4 * sisi4));
        double jarakDatabase = perbandingan.jarak.toDouble();
        double jarakDatabase2 = perbandingan.jarak2.toDouble();
        double jarakDatabase3 = perbandingan.jarak3.toDouble();
        double jarakDatabase4 = perbandingan.jarak4.toDouble();
        double jarakDatabase5 = perbandingan.jarak5.toDouble();
        double jarakDatabase6 = perbandingan.jarak6.toDouble();
        double jarakDatabase7 = perbandingan.jarak7.toDouble();
        double jarakDatabase8 = perbandingan.jarak8.toDouble();

        print('testing');
        print(poseLandmark1.type);
        print(poseLandmark1.x);
        print(poseLandmark1.y);

        print(poseLandmark2.type);
        print(poseLandmark2.x);
        print(poseLandmark2.y);
        print('testing2');
        print(poseLandmark3.type);
        print(poseLandmark3.x);
        print(poseLandmark3.y);

        print(poseLandmark4.type);
        print(poseLandmark4.x);
        print(poseLandmark4.y);

        double batas2_jarak2 = perbandingan.batas2Jarak2.toDouble();
        double batas3_jarak3 = perbandingan.batas3Jarak3.toDouble();
        double batas4_jarak4 = perbandingan.batas4Jarak4.toDouble();
        double batas5_jarak5 = perbandingan.batas5Jarak5.toDouble();
        double batas6_jarak6 = perbandingan.batas6Jarak6.toDouble();
        double batas7_jarak7 = perbandingan.batas7Jarak7.toDouble();
        var batas1 = 100;
        var batas2 = 120;
        var batas3 = 140;
        var batas4 = 150;
        var batas5 = 160;
        const batas = 200;
        if (jarakReal.round() >= 250 && jarakReal.round() <= jarakDatabase) {
          if (jarakReal2.round() >= jarakDatabase2 &&
              jarakReal2.round() < jarakDatabase3) {
            var c11 = 'Gerakan Virabhadrasana2 F';
            updateState(c11);
            print("Gerakan Virabhadrasana2 F");
            statusBenar6 = true;
          }
        } else if (jarakReal.round() >= jarakDatabase &&
            jarakReal.round() < batas2_jarak2) {
          if (jarakReal2.round() >= jarakDatabase3 &&
              jarakReal2.round() < jarakDatabase4) {
            var c11 = 'Gerakan Virabhadrasana2 E';
            updateState(c11);
            print("Gerakan Virabhadrasana2 E");
            statusBenar5 = true;
          }
        } else if (jarakReal.round() >= batas2_jarak2 &&
            jarakReal.round() < batas3_jarak3) {
          if (jarakReal2.round() >= jarakDatabase4 &&
              jarakReal2.round() < jarakDatabase5) {
            var c13 = 'Gerakan Virabhadrasana2 D';
            updateState(c13);
            print("Gerakan Virabhadrasana2 D");
            statusBenar4 = true;
          }
        } else if (jarakReal.round() >= batas3_jarak3 &&
            jarakReal.round() < batas4_jarak4) {
          if (jarakReal2.round() >= jarakDatabase5 &&
              jarakReal2.round() < jarakDatabase6) {
            var c14 = 'Gerakan Virabhadrasana2 C';
            updateState(c14);
            print("Gerakan Virabhadrasana2 C");
            statusBenar3 = true;
          }
        } else if (jarakReal.round() >= batas4_jarak4 &&
            jarakReal.round() < batas5_jarak5) {
          if (jarakReal2.round() >= jarakDatabase6 &&
              jarakReal2.round() < jarakDatabase7) {
            var c15 = 'Gerakan Virabhadrasana2 B';
            updateState(c15);
            print("Gerakan Virabhadrasana2 B");
            statusBenar2 = true;
          }
        } else if (jarakReal.round() >= batas5_jarak5 &&
            jarakReal.round() <= batas7_jarak7) {
          if (jarakReal2.round() >= jarakDatabase7 &&
              jarakReal2.round() <= jarakDatabase8) {
            var c15 = 'Gerakan Virabhadrasana2 A';
            updateState(c15);
            print("Gerakan Virabhadrasana2 A");
            statusBenar = true;
          }
        }
        // else if (jarakReal.round() <= jarakDatabase) {
        //   // if (jarakReal2.round() <= jarakDatabase2 &&
        //   //     jarakReal2.round() > 377) {
        //   var c15 = 'Gerakan Virabhadrasana2 F';
        //   updateState(c15);
        //   print("Gerakan Virabhadrasana2 F");
        //   statusBenar5 = true;
        //   //}
        // }

        //  else if (jarakReal.round() <= jarakDatabase &&
        //     jarakReal.round() >= 100) {
        //   if (jarakReal2.round() > jarakDatabase8 &&
        //       jarakReal2.round() <= 600) {
        //     var c15 = 'Gerakan Virabhadrasana2 G';
        //     updateState(c15);
        //     print("Gerakan Virabhadrasana2 G");
        //     //statusBenar5 = true;
        //   }
        // }
        else if (jarakReal.round() > jarakDatabase7) {
          print("Gerakan yang lain");
        }
      }
    }

    if (inputImage.inputImageData?.size != null &&
        inputImage.inputImageData?.imageRotation != null) {
      final painter = PosePainter(data_poses, inputImage.inputImageData!.size,
          inputImage.inputImageData!.imageRotation);
      customPaint = CustomPaint(painter: painter);
    } else {
      _text = 'Poses found: ${data_poses.length}\n\n';
      // TODO: set _customPaint to draw landmarks on top of image
      customPaint = null;
    }
    _isBusy = false;
    if (mounted) {
      setState(() {});
    }
    if (statusBenar == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar2 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar3 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar4 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar5 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar6 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }

    void nilaiPose() {}
    ;
  }

  getData2() {
    player.play(AssetSource('dummy/countdown_flutter.mp3'));
  }

  pause() async {
    await player.dispose();
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        if (statusBenar == true) {
          return virabhadrasana2A();
        } else if (statusBenar2 == true) {
          return virabhadrasana2B();
        } else if (statusBenar3 == true) {
          return virabhadrasana2C();
        } else if (statusBenar4 == true) {
          return virabhadrasana2D();
        } else if (statusBenar5 == true) {
          return virabhadrasana2E();
        } else if (statusBenar6 == true) {
          return virabhadrasana2F();
        } else {
          return virabhadrasana2E();
        }
      }),
    );
    getData();
  }

  getData() async {
    player2.play(AssetSource('dummy/correct.mp3'));
    await player2.dispose();
  }

  benar(var c) {
    a = 'benar';
    c = a;
    updateState(c);
    print('${c}');
  }
}
