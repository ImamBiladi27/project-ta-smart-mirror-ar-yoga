import 'dart:async';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view2.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming_up2.dart';
import 'package:video_player/video_player.dart';

import '../detector_views.dart';
import 'warming4_up2.dart';

void main() => runApp(const Video4PlayerScreen1());

class Video4PlayerScreen1 extends StatefulWidget {
  const Video4PlayerScreen1({Key? key}) : super(key: key);

  @override
  State<Video4PlayerScreen1> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<Video4PlayerScreen1> {
  final FlickManager flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.asset(
    'assets/dummy/1.mp4',
  ));

  //late VideoPlayerController _controller;
  // late Future<void> _initializeVideoPlayerFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              setState(() {
                flickManager.flickControlManager?.pause();
              });
              Navigator.of(context).pop();
            }),
        title: const Text('Session 1'),
      ),
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: Center(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: FlickVideoPlayer(flickManager: flickManager),
            ),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    flickManager.flickControlManager?.pause();
                  });
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Video4PlayerScreen2()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
