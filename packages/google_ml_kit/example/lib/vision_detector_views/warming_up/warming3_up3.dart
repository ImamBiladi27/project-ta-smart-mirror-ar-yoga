import 'dart:async';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view2.dart';
import 'package:video_player/video_player.dart';

import '../detector_views.dart';
import '../pose_detector_view3.dart';

void main() => runApp(const Video3PlayerScreen3());

class Video3PlayerScreen3 extends StatefulWidget {
  const Video3PlayerScreen3({Key? key}) : super(key: key);

  @override
  State<Video3PlayerScreen3> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<Video3PlayerScreen3> {
  final FlickManager flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.asset(
    'assets/dummy/3.mp4',
  ));

  //late VideoPlayerController _controller;
  // late Future<void> _initializeVideoPlayerFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              setState(() {
                flickManager.flickControlManager?.pause();
              });
              Navigator.of(context).pop();
            }),
        title: const Text('Session 3'),
      ),
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: Center(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: FlickVideoPlayer(flickManager: flickManager),
            ),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    flickManager.flickControlManager?.pause();
                  });
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => PoseDetectorView3()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
