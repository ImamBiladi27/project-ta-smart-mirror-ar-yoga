import 'dart:async';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view2.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming4_up3%20.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming_up3.dart';
import 'package:video_player/video_player.dart';

import '../detector_views.dart';

void main() => runApp(const Video4PlayerScreen2());

class Video4PlayerScreen2 extends StatefulWidget {
  const Video4PlayerScreen2({Key? key}) : super(key: key);

  @override
  State<Video4PlayerScreen2> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<Video4PlayerScreen2> {
  final FlickManager flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.asset(
    'assets/dummy/2.mp4',
  ));

  //late VideoPlayerController _controller;
  // late Future<void> _initializeVideoPlayerFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              setState(() {
                flickManager.flickControlManager?.pause();
              });
              Navigator.of(context).pop();
            }),
        title: const Text('Session 2'),
      ),
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: Center(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: FlickVideoPlayer(flickManager: flickManager),
            ),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    flickManager.flickControlManager?.pause();
                  });
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Video4PlayerScreen3()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
