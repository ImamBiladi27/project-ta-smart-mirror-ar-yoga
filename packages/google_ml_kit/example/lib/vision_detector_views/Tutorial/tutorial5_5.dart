import 'dart:async';

import 'package:flick_video_player/flick_video_player.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial1_4.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial1_6.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial2_6.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial3_6.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial4_6.dart';
import 'package:google_ml_kit_example/vision_detector_views/Tutorial/tutorial5_6.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view2.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming_up2.dart';
import 'package:video_player/video_player.dart';

import '../detector_views.dart';

void main() => runApp(const Tutorial5_5());

class Tutorial5_5 extends StatefulWidget {
  const Tutorial5_5({Key? key}) : super(key: key);

  @override
  State<Tutorial5_5> createState() => _VideoPlayerScreenState();
}

class _VideoPlayerScreenState extends State<Tutorial5_5> {
  final FlickManager flickManager = FlickManager(
      videoPlayerController: VideoPlayerController.asset(
    'assets/dummy/vrikshasana.mp4',
  ));

  //late VideoPlayerController _controller;
  // late Future<void> _initializeVideoPlayerFuture;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () {
              setState(() {
                flickManager.flickControlManager?.pause();
              });
              Navigator.of(context).pop();
            }),
        title: const Text('Gerakan Vriksashana'),
      ),
      // Use a FutureBuilder to display a loading spinner while waiting for the
      // VideoPlayerController to finish initializing.
      body: SafeArea(
        child: Column(
          children: [
            AspectRatio(
              aspectRatio: 16 / 9,
              child: FlickVideoPlayer(flickManager: flickManager),
            ),
            SizedBox(width: 20),
            Center(
              child: Container(
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      flickManager.flickControlManager?.pause();
                    });
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Tutorial5_6()));
                  },
                  child: const Text('Next'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
