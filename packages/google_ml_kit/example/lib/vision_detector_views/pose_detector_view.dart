import 'dart:async';
import 'dart:math';
import 'package:audioplayers/audioplayers.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:google_mlkit_pose_detection/google_mlkit_pose_detection.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:simple_knn/simple_knn.dart';
import '../main.dart';
import '../model/padahastana.dart';
import '../providers/pose_provider.dart';

import 'camera_view.dart';
import 'painters/pose_painter.dart';
import 'package:typed_data/typed_data.dart';
import 'package:data/data.dart';
import 'package:google_ml_kit_example/model/padahastana.dart';

import 'second_route.dart';

class PoseDetectorView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _PoseDetectorViewState();
}

class _PoseDetectorViewState extends State<PoseDetectorView> {
  PoseDetector _poseDetector = PoseDetector(options: PoseDetectorOptions());
  //final audioPlayer = AudioPlayer();
  bool isPlaying = false;
  Duration duration = Duration.zero;

  bool _isBusy = false;
  CustomPaint? customPaint;
  String? _text;
  String a = 'salah';
  //get b => a;
  final player = AudioPlayer();

  final player2 = AudioPlayer();
  var statusBenar = null;
  var statusBenar2 = null;
  var statusBenar3 = null;
  var statusBenar4 = null;
  var statusBenar5 = null;
  var statusBenar6 = null;
  @override
  void dispose() async {
    //_canProcess = false;

    super.dispose();
    await _poseDetector.close();
  }

  void updateState(String type) {
    // setState(() {
    a = type;
    // });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Stack(
          children: <Widget>[
            CameraView(
              title: 'Padahastana',
              image: Image.asset('assets/padahastana.jpg'),
              customPaint: customPaint,
              onImage: (inputImage) {
                //_startTimer();
                processImage(inputImage, context);
                a = '';
                // processImage2(inputImage);
              },
            ),
            Positioned(
                top: 90,
                right: 1,
                child: Container(
                  width: 100,
                  height: 100,
                  child: Card(child: Image.asset('assets/padahastana.jpg')),
                )),
            FutureBuilder<String>(
              //future: processImage(param1),
              builder: (BuildContext context, AsyncSnapshot snapshot) =>
                  Positioned(
                bottom: 170,
                left: 100,
                //leading:Image.asset('name'),
                child: LinearPercentIndicator(
                  width: 250,
                  lineHeight: 30,
                  percent: 0.0,
                  center: Text(
                    //  "${(double.parse(percent))}",
                    '${a}',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 10),
                  ),
                  linearStrokeCap: LinearStrokeCap.roundAll,
                  progressColor: Colors.greenAccent,
                  backgroundColor: Colors.white,
                  animation: true,
                  animationDuration: 5000,
                ),
              ),
            )
          ],
        ),
        builder: EasyLoading.init());
  }

  Future<void> processImage(
    InputImage inputImage,
    context,
  ) async {
    PoseProvider provider = new PoseProvider();
    String b2 = "";
    //  player.play(UrlSource('note1.wave'));

// If file located in assets folder like assets/sounds/note01.wave"

    //await player.play(AssetSource('assets/dummy/countdown_flutter.mp3'));
    if (_isBusy) return;
    _isBusy = true;
    var _arrLandmark = [];
    List<Padahastana> getPadahastana = await PoseProvider.getPadahastana1();

    List<PoseLandmark> listPL = [];
    List<Padahastana> listpada = [];
    List<PoseLandmark> listLandmark = [];
    List<String> listType = [];
    var batas = 100;
    final data_poses = await _poseDetector.processImage(inputImage);

    for (Pose pose in data_poses) {
      pose.landmarks.forEach((_, landmark) {
        List _landmarkXY = [];
        String type = landmark.type.toString();

        //String type2 = landmark.type.toString();
        final x = landmark.x;
        final y = landmark.y;

        print('sinkron getpadahastana');
        for (var i = 0; i < getPadahastana.length; i++) {
          Padahastana sinkron = getPadahastana[i];

          if (type == sinkron.key1 ||
              type == sinkron.key2 ||
              type == sinkron.key3 ||
              type == sinkron.key4) {
            listLandmark.add(landmark);
            print('masuk1');
          }
        }
      });
      final landmark = pose.landmarks[PoseLandmarkType.nose];
    }

    print('list $listLandmark');
    //print('ini listType $listType');

    var statusBenar = null;
    var statusBenar2 = null;
    var statusBenar3 = null;
    var statusBenar4 = null;
    var statusBenar5 = null;
    for (final e1 in listLandmark) {
      for (Padahastana perbandingan in getPadahastana) {
        PoseLandmark poseLandmark1 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key1)
            .toList()[0];

        PoseLandmark poseLandmark2 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key2)
            .toList()[0];

        PoseLandmark poseLandmark3 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key3)
            .toList()[0];
        PoseLandmark poseLandmark4 = listLandmark
            .where((e1) => e1.type.toString() == perbandingan.key4)
            .toList()[0];

        double sisi1;
        double sisi2;
        double sisi3;
        double sisi4;
        if (poseLandmark1.x > poseLandmark2.x) {
          sisi1 = poseLandmark1.x - poseLandmark2.x;
        } else {
          sisi1 = poseLandmark2.x - poseLandmark1.x;
        }
        if (poseLandmark3.x > poseLandmark4.x) {
          sisi3 = poseLandmark3.x - poseLandmark4.x;
        } else {
          sisi3 = poseLandmark4.x - poseLandmark3.x;
        }
        if (poseLandmark1.y > poseLandmark2.y) {
          sisi2 = poseLandmark1.y - poseLandmark2.y;
        } else {
          sisi2 = poseLandmark2.y - poseLandmark1.y;
        }
        if (poseLandmark3.y > poseLandmark4.y) {
          sisi4 = poseLandmark3.y - poseLandmark4.y;
        } else {
          sisi4 = poseLandmark4.y - poseLandmark3.y;
        }

        ///pytagoras
        double jarakReal = sqrt((sisi1 * sisi1) + (sisi2 * sisi2));

        //normalisasi
        //double jarakNormalisasi = jarakReal/100;
        double jarakReal2 = sqrt((sisi3 * sisi3) + (sisi4 * sisi4));
        double jarakDatabase = perbandingan.jarak.toDouble();
        double jarakDatabase2 = perbandingan.jarak2.toDouble();
        double jarakDatabase3 = perbandingan.batas.toDouble();
        double jarakDatabase4 = perbandingan.batas2.toDouble();
        double jarakDatabase5 = perbandingan.batas3.toDouble();
        double jarakDatabase6 = perbandingan.batas4.toDouble();
        double jarakDatabase7 = perbandingan.batas5.toDouble();

        print('testing');
        print(poseLandmark1.type);
        print(poseLandmark1.x);
        print(poseLandmark1.y);

        print(poseLandmark2.type);
        print(poseLandmark2.x);
        print(poseLandmark2.y);
        print('testing2');
        print(poseLandmark3.type);
        print(poseLandmark3.x);
        print(poseLandmark3.y);

        print(poseLandmark4.type);
        print(poseLandmark4.x);
        print(poseLandmark4.y);
        var batas1 = 40;
        var batas2 = 120;
        var batas3 = 140;
        var batas4 = 150;
        var batas5 = 160;
        var batas6 = 180;
        var batas7 = 200;
        if (jarakReal.round() <= jarakDatabase && jarakReal.round() >= batas1) {
          if (jarakReal2.round() >= jarakDatabase2 &&
              jarakReal2.round() < jarakDatabase3) {
            var c11 = 'Gerakan Padahastana A';
            updateState(c11);
            print("Gerakan Padahastana A");
            statusBenar = true;
          }
        } else if (jarakReal.round() > jarakDatabase &&
            jarakReal.round() <= batas2) {
          if (jarakReal2.round() >= jarakDatabase3 &&
              jarakReal2.round() < jarakDatabase4) {
            var c122 = 'Gerakan Padahastana B';
            updateState(c122);
            print("Gerakan Padahastana B");
            statusBenar2 = true;
          }
        } else if (jarakReal.round() > batas2 && jarakReal.round() <= batas3) {
          if (jarakReal2.round() >= jarakDatabase4 &&
              jarakReal2.round() < jarakDatabase5) {
            var c13 = 'Gerakan Padahastana C';
            updateState(c13);
            print("Gerakan Padahastana C");
            statusBenar3 = true;
          }
        } else if (jarakReal.round() > batas3 && jarakReal.round() <= batas4) {
          if (jarakReal2.round() >= jarakDatabase5 &&
              jarakReal2.round() < jarakDatabase6) {
            var c14 = 'Gerakan Padahastana D';
            updateState(c14);
            print("Gerakan Padahastana D");
            statusBenar4 = true;
          }
        } else if (jarakReal.round() > batas4 && jarakReal.round() <= batas5) {
          if (jarakReal2.round() >= jarakDatabase7 &&
              jarakReal2.round() < 220) {
            var c15 = 'Gerakan Padahastana E';
            updateState(c15);
            print("Gerakan Padahastana E");
            statusBenar5 = true;
          }
        } else if (jarakReal.round() > batas5 && jarakReal.round() <= batas6) {
          if (jarakReal2.round() >= jarakDatabase7 &&
              jarakReal2.round() < 250) {
            var c16 = 'Gerakan Padahastana F';
            updateState(c16);
            print("Gerakan Padahastana F");
            statusBenar6 = true;
          }
        } else if (jarakReal.round() > jarakDatabase) {
          print("Gerakan yang lain");
        }
      }
    }

    if (inputImage.inputImageData?.size != null &&
        inputImage.inputImageData?.imageRotation != null) {
      final painter = PosePainter(data_poses, inputImage.inputImageData!.size,
          inputImage.inputImageData!.imageRotation);
      customPaint = CustomPaint(painter: painter);
    } else {
      _text = 'Poses found: ${data_poses.length}\n\n';
      // TODO: set _customPaint to draw landmarks on top of image
      customPaint = null;
    }
    _isBusy = false;
    if (mounted) {
      setState(() {});
    }
    if (statusBenar == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar2 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar3 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar4 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar5 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    if (statusBenar6 == true) {
      getData2();
      var counter = 1;
      Timer.periodic(const Duration(seconds: 6), (timer) {
        print(timer.tick);
        setState(() {});
        counter--;
        if (counter == 0) {
          pause();
          print('Cancel timer');
          timer.cancel();
        }
      });
    }
    ;
  }

  getData2() {
    player.play(AssetSource('dummy/countdown_flutter.mp3'));
  }

  pause() async {
    await player.dispose();
    Navigator.of(context).pop();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) {
        if (statusBenar == true) {
          return OneRoute();
        } else if (statusBenar2 == true) {
          return SecondRoute();
        } else if (statusBenar3 == true) {
          return ThirdRoute();
        } else if (statusBenar4 == true) {
          return FourthRoute();
        } else if (statusBenar5 == true) {
          return FifthRoute();
        } else if (statusBenar6 == true) {
          return SixRoute();
        } else {
          return OneRoute();
        }
      }),
    );
    getData();
  }

  getData() async {
    player2.play(AssetSource('dummy/correct.mp3'));
    await player2.dispose();
  }

  benar(var c) {
    a = 'benar';
    c = a;
    updateState(c);
    print('${c}');
  }
}
