import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'pose_detector_view.dart';

class OneRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            // SizedBox.expand(
            //   child: Center(
            //     child: Text(
            //       'Score',
            //       style: TextStyle(color: Colors.black),
            //     ), //Text
            //   ), //Card
            // ), //
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Padahastana A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Padahastana B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ThirdRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Padahastana C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FourthRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Padahastana D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FifthRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Padahastana E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SixRoute extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Padahastana F'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana extends StatelessWidget {
  final a = '';
  final status = null;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            // SizedBox.expand(
            //   child: Center(
            //     child: Text(
            //       'Score',
            //       style: TextStyle(color: Colors.black),
            //     ), //Text
            //   ), //Card
            // ), //
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Bhujangasana A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana2 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            // SizedBox.expand(
            //   child: Center(
            //     child: Text(
            //       'Score',
            //       style: TextStyle(color: Colors.black),
            //     ), //Text
            //   ), //Card
            // ), //
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Bhujangasana B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana3 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Bhujangasana C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana4 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Bhujangasana D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana5 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Bhujangasana E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class bhujangasana6 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Bhujangasana F'),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1A extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Virabhadrasana1 A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1B extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Virabhadrasana1 B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1C extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Virabhadrasana1 C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1D extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Virabhadrasana1 D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1E extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Virabhadrasana1 E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana1F extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Virabhadrasana1 F'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2A extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Virabhadrasana2 A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2B extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Virabhadrasana2 B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2C extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Virabhadrasana2 C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2D extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Virabhadrasana2 D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2E extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Virabhadrasana2 E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana2F extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Virabhadrasana2 F'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3A extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Virabhadrasana3 A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3B extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Virabhadrasana3 B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3C extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Virabhadrasana3 C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3D extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Virabhadrasana3 D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3E extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Virabhadrasana3 E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class virabhadrasana3F extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Virabhadrasana3 F'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('100'),
            Text('Gerakan Vriksasana A'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana2 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('90'),
            Text('Gerakan Vriksasana B'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana3 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('80'),
            Text('Gerakan Vriksasana C'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana4 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('70'),
            Text('Gerakan Vriksasana D'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana5 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('60'),
            Text('Gerakan Vriksasana E'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class vriksasana6 extends StatelessWidget {
  final a = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Hasil Score'),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Score Anda'),
            Text('50'),
            Text('Gerakan Vriksasana F'),
            //  Text(p.updateState(a)),
            Container(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Home()));
                },
                child: const Text('Next'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
