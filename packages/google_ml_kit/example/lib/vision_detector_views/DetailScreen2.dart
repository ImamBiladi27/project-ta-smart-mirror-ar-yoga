import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming2_up2.dart';
import 'package:google_ml_kit_example/vision_detector_views/warming_up/warming2_up3.dart';

import 'Tutorial/tutorial1_2.dart';
import 'warming_up/warming2_up1.dart';
import 'warming_up/warming_up.dart';

class DetailsScreen2 extends StatelessWidget {
  final ButtonStyle style =
      ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: size.height * 0.05,
                    ),
                    SizedBox(height: 10),
                    Text(
                      "3-10 MIN Course",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    SizedBox(
                      width: size.width * .7, // it just take 60% of total width
                      child: Text(
                        "Smart Mirror Asisten Latihan Yoga Anda",
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Wrap(
                      spacing: 20,
                      runSpacing: 20,
                      children: <Widget>[
                        SeassionCard(
                            seassionNum: 1,
                            isDone: true,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const Video2PlayerScreen1()),
                              );
                            }),
                        SeassionCard(
                            seassionNum: 2,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const Video2PlayerScreen2()),
                              );
                            }),
                        SeassionCard(
                            seassionNum: 3,
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const Video2PlayerScreen3()),
                              );
                            }),
                      ],
                    ),
                    SizedBox(height: 50),
                    Center(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ElevatedButton(
                            style: style,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          Video2PlayerScreen1()));
                            },
                            child: const Text('Mulai Latihan'),
                          ),
                          const SizedBox(height: 10),
                          ElevatedButton(
                            style: style,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Tutorial2()));
                            },
                            child: const Text('Tutorial Gerakan'),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SeassionCard extends StatelessWidget {
  final int seassionNum;
  final bool isDone;

  const SeassionCard({
    Key? key,
    required this.seassionNum,
    this.isDone = false,
    required Null Function() onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(13),
        child: Container(
          width: constraint.maxWidth / 2 -
              10, // constraint.maxWidth provide us the available with for this widget
          // padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(13),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 17),
                blurRadius: 23,
                spreadRadius: -13,
                color: Colors.black,
              ),
            ],
          ),
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 42,
                      width: 43,
                      decoration: BoxDecoration(
                        color: isDone ? Colors.blue : Colors.white,
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.blue),
                      ),
                      child: Icon(
                        Icons.play_arrow,
                        color: isDone ? Colors.white : Colors.blue,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text('Sesion $seassionNum '),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
