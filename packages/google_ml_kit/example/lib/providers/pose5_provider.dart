import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/virabhadrasana3.dart';

class Pose5Provider {
  //PoseProvider._();
  List alldata = [
    // {228, 456},
    // {290, 456},
    // {233, 523},
    // {205, 376},
    // {335, 318},
    {"id": 1},
  ];
  static Future<List<Virabhadrasana3>> getVirabhadrasana3() async {
    final json =
        await rootBundle.loadString('assets/dummy/virabhadrasana3baru.json');
    final response = await jsonDecode(json);
    // print('ini provider Bhujangasana');
    // print(json);
    // print(response);
    return List<Virabhadrasana3>.from(
        response['data'].map((element) => Virabhadrasana3.fromJson(element)));
  }
}
