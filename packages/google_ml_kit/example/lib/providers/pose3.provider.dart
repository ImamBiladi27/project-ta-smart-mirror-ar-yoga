import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/virabhadrasana1.dart';

class Pose3Provider {
  //PoseProvider._();
  List alldata = [
    // {228, 456},
    // {290, 456},
    // {233, 523},
    // {205, 376},
    // {335, 318},
    {"id": 1},
  ];
  static Future<List<Virabhadrasana1>> getVirabhadrasana1() async {
    final json =
        await rootBundle.loadString('assets/dummy/virabhadrasana1baru.json');
    final response = await jsonDecode(json);
    // print('ini provider Bhujangasana');
    // print(json);
    // print(response);
    return List<Virabhadrasana1>.from(
        response['data'].map((element) => Virabhadrasana1.fromJson(element)));
  }
}
