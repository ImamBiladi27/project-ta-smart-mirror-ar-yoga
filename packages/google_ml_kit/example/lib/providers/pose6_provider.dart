import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/viraksana.dart';

class Pose6Provider {
  //PoseProvider._();
  List alldata = [
    // {228, 456},
    // {290, 456},
    // {233, 523},
    // {205, 376},
    // {335, 318},
    {"id": 1},
  ];
  static Future<List<Viraksana>> getViraksana() async {
    final json = await rootBundle.loadString('assets/dummy/viraksana.json');
    final response = await jsonDecode(json);
    // print('ini provider Bhujangasana');
    // print(json);
    // print(response);
    return List<Viraksana>.from(
        response['data'].map((element) => Viraksana.fromJson(element)));
  }
}
