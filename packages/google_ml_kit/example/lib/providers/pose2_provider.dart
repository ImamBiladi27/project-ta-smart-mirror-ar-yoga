import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/bhujangasana.dart';
import '../model/padahastana.dart';

class Pose2Provider {
  //PoseProvider._();
  List alldata = [
    // {228, 456},
    // {290, 456},
    // {233, 523},
    // {205, 376},
    // {335, 318},
    {"id": 1},
  ];
  static Future<List<Bhujangasana>> getBhujangasana() async {
    final json =
        await rootBundle.loadString('assets/dummy/bhujangasana1baru.json');
    final response = await jsonDecode(json);
    // print('ini provider Bhujangasana');
    // print(json);
    // print(response);
    return List<Bhujangasana>.from(
        response['data'].map((element) => Bhujangasana.fromJson(element)));
  }
}
