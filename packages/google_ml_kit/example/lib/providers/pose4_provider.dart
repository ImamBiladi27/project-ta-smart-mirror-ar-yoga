import 'dart:convert';

import 'package:flutter/services.dart';

import '../model/virabhadrasana2.dart';

class Pose4Provider {
  //PoseProvider._();
  List alldata = [
    // {228, 456},
    // {290, 456},
    // {233, 523},
    // {205, 376},
    // {335, 318},
    {"id": 1},
  ];
  static Future<List<Virabhadrasana2>> getVirabhadrasana2() async {
    final json =
        await rootBundle.loadString('assets/dummy/virabhadrasana2baru.json');
    final response = await jsonDecode(json);
    // print('ini provider Bhujangasana');
    // print(json);
    // print(response);
    return List<Virabhadrasana2>.from(
        response['data'].map((element) => Virabhadrasana2.fromJson(element)));
  }
}
