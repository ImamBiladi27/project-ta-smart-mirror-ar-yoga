// // // To parse this JSON data, do
// // //
// // //     final bhujangasana = bhujangasanaFromJson(jsonString);

// // import 'dart:convert';

// // List<Bhujangasana> bhujangasanaFromJson(String str) => List<Bhujangasana>.from(
// //     json.decode(str).map((x) => Bhujangasana.fromJson(x)));

// // String bhujangasanaToJson(List<Bhujangasana> data) =>
// //     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// // class Bhujangasana {
// //   Bhujangasana({
// //     required this.key,
// //     required this.no,
// //     required this.valueX,
// //     required this.valueY,
// //     required this.akar,
// //   });

// //   String key;
// //   int no;
// //   double valueX;
// //   double valueY;
// //   double akar;
// //   factory Bhujangasana.fromJson(Map<String, dynamic> json) => Bhujangasana(
// //         key: json["key"],
// //         no: json["no"],
// //         valueX: json["value_x"],
// //         valueY: json["value_y"],
// //         akar: json["akar"]?.toDouble(),
// //       );

// //   Map<String, dynamic> toJson() => {
// //         "key": key,
// //         "no": no,
// //         "value_x": valueX,
// //         "value_y": valueY,
// //         "akar": akar,
// //       };
// // }

// // To parse this JSON data, do
// //
// //     final bhujangasana = bhujangasanaFromJson(jsonString);

// // import 'dart:convert';

// // List<Bhujangasana> bhujangasanaFromJson(String str) => List<Bhujangasana>.from(
// //     json.decode(str).map((x) => Bhujangasana.fromJson(x)));

// // String bhujangasanaToJson(List<Bhujangasana> data) =>
// //     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// // class Bhujangasana {
// //   String key1;
// //   String key2;
// //   int jarak;

// //   Bhujangasana({
// //     required this.key1,
// //     required this.key2,
// //     required this.jarak,
// //   });

// //   factory Bhujangasana.fromJson(Map<String, dynamic> json) => Bhujangasana(
// //         key1: json["key1"],
// //         key2: json["key2"],
// //         jarak: json["jarak"],
// //       );

// //   Map<String, dynamic> toJson() => {
// //         "key1": key1,
// //         "key2": key2,
// //         "jarak": jarak,
// //       };
// // }

// // To parse this JSON data, do
// //
// //     final bhujangasana = bhujangasanaFromJson(jsonString);

// import 'dart:convert';

// List<Bhujangasana> bhujangasanaFromJson(String str) => List<Bhujangasana>.from(
//     json.decode(str).map((x) => Bhujangasana.fromJson(x)));

// String bhujangasanaToJson(List<Bhujangasana> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Bhujangasana {
//   String key1;
//   String key2;
//   int jarak;
//   String key3;
//   String key4;
//   int jarak2;

//   Bhujangasana({
//     required this.key1,
//     required this.key2,
//     required this.jarak,
//     required this.key3,
//     required this.key4,
//     required this.jarak2,
//   });

//   factory Bhujangasana.fromJson(Map<String, dynamic> json) => Bhujangasana(
//         key1: json["key1"],
//         key2: json["key2"],
//         jarak: json["jarak"],
//         key3: json["key3"],
//         key4: json["key4"],
//         jarak2: json["jarak2"],
//       );

//   Map<String, dynamic> toJson() => {
//         "key1": key1,
//         "key2": key2,
//         "jarak": jarak,
//         "key3": key3,
//         "key4": key4,
//         "jarak2": jarak2,
//       };
// }

// To parse this JSON data, do
//
//     final bhujangasana = bhujangasanaFromJson(jsonString);

import 'dart:convert';

List<Bhujangasana> bhujangasanaFromJson(String str) => List<Bhujangasana>.from(
    json.decode(str).map((x) => Bhujangasana.fromJson(x)));

String bhujangasanaToJson(List<Bhujangasana> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Bhujangasana {
  String key1;
  String key2;
  int jarak;
  int batas2Jarak2;
  int batas3Jarak3;
  int batas4Jarak4;
  int batas5Jarak5;
  int batas6Jarak6;
  int batas7Jarak7;
  String key3;
  String key4;
  int jarak2;
  int jarak3;
  int jarak4;
  int jarak5;
  int jarak6;
  int jarak7;
  int jarak8;

  Bhujangasana({
    required this.key1,
    required this.key2,
    required this.jarak,
    required this.batas2Jarak2,
    required this.batas3Jarak3,
    required this.batas4Jarak4,
    required this.batas5Jarak5,
    required this.batas6Jarak6,
    required this.batas7Jarak7,
    required this.key3,
    required this.key4,
    required this.jarak2,
    required this.jarak3,
    required this.jarak4,
    required this.jarak5,
    required this.jarak6,
    required this.jarak7,
    required this.jarak8,
  });

  factory Bhujangasana.fromJson(Map<String, dynamic> json) => Bhujangasana(
        key1: json["key1"],
        key2: json["key2"],
        jarak: json["jarak"],
        batas2Jarak2: json["batas2_jarak2"],
        batas3Jarak3: json["batas3_jarak3"],
        batas4Jarak4: json["batas4_jarak4"],
        batas5Jarak5: json["batas5_jarak5"],
        batas6Jarak6: json["batas6_jarak6"],
        batas7Jarak7: json["batas7_jarak7"],
        key3: json["key3"],
        key4: json["key4"],
        jarak2: json["jarak2"],
        jarak3: json["jarak3"],
        jarak4: json["jarak4"],
        jarak5: json["jarak5"],
        jarak6: json["jarak6"],
        jarak7: json["jarak7"],
        jarak8: json["jarak8"],
      );

  Map<String, dynamic> toJson() => {
        "key1": key1,
        "key2": key2,
        "jarak": jarak,
        "batas2_jarak2": batas2Jarak2,
        "batas3_jarak3": batas3Jarak3,
        "batas4_jarak4": batas4Jarak4,
        "batas5_jarak5": batas5Jarak5,
        "batas6_jarak6": batas6Jarak6,
        "batas7_jarak7": batas7Jarak7,
        "key3": key3,
        "key4": key4,
        "jarak2": jarak2,
        "jarak3": jarak3,
        "jarak4": jarak4,
        "jarak5": jarak5,
        "jarak6": jarak6,
        "jarak8": jarak8,
      };
}
