class dataTraining {
  int? id;
  String? key1;
  String? key2;
  double? jarak;
  String? key3;
  String? key4;
  int? jarak2;
  int? batas;
  int? batas2;
  int? batas3;
  int? batas4;
  int? batas5;
  dataTraining({
    this.id,
    this.key1,
    this.key2,
    this.jarak,
    this.key3,
    this.key4,
    this.jarak2,
    this.batas,
    this.batas2,
    this.batas3,
    this.batas4,
    this.batas5,
  });

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    } else if (jarak != null) {
      map['jarak'] = jarak;
    } else if (jarak2 != null) {
      map['jarak2'] = jarak2;
    } else if (batas != null) {
      map['batas'] = batas;
    } else if (batas2 != null) {
      map['batas2'] = batas2;
    } else if (batas3 != null) {
      map['batas3'] = batas3;
    } else if (batas4 != null) {
      map['batas4'] = batas4;
    } else if (batas5 != null) {
      map['batas5'] = batas5;
    }
    map['key1'] = key1;
    map['key2'] = key2;
    map['key3'] = key3;
    map['key4'] = key4;
    return map;
  }

  dataTraining.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.key1 = map['key1'];
    this.key2 = map['key2'];
    this.jarak = map['jarak'];
    this.key3 = map['key3'];
    this.key4 = map['key4'];
    this.jarak2 = map['jarak2'];
    this.batas = map['batas'];
    this.batas2 = map['batas2'];
    this.batas3 = map['batas3'];
    this.batas4 = map['batas4'];
    this.batas5 = map['batas5'];
  }
}
