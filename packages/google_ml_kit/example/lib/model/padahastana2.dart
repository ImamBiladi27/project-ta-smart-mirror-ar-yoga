// To parse this JSON data, do

//     final padahastana2 = padahastana2FromJson(jsonString);

import 'dart:convert';

List<Padahastana2> padahastana2FromJson(String str) => List<Padahastana2>.from(
    json.decode(str).map((x) => Padahastana2.fromJson(x)));

String padahastanaToJson(List<Padahastana2> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Padahastana2 {
  Padahastana2(
      {required this.key,
      required this.no,
      required this.valueX,
      required this.valueY,
      required this.akar});

  String key;
  int no;
  int valueX;
  int valueY;
  double akar;

  factory Padahastana2.fromJson(Map<String, dynamic> json) => Padahastana2(
        key: json["key"],
        no: json["no"],
        valueX: json["value_x"],
        valueY: json["value_y"],
        akar: json["akar"]?.toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "no": no,
        "value_x": valueX,
        "value_y": valueY,
        "akar": akar,
      };
}

// To parse this JSON data, do
//
//     final padahastana2 = padahastana2FromJson(jsonString);

// import 'dart:convert';

// List<Padahastana2> padahastana2FromJson(String str) => List<Padahastana2>.from(
//     json.decode(str).map((x) => Padahastana2.fromJson(x)));

// String padahastana2ToJson(List<Padahastana2> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Padahastana2 {
//   Padahastana2({
//     required this.data1,
//   });

//   List<Data1> data1;

//   factory Padahastana2.fromJson(Map<String, dynamic> json) => Padahastana2(
//         data1: List<Data1>.from(json["data1"].map((x) => Data1.fromJson(x))),
//       );

//   Map<String, dynamic> toJson() => {
//         "data1": List<dynamic>.from(data1.map((x) => x.toJson())),
//       };
// }

// class Data1 {
//   Data1({
//     required this.key,
//     required this.no,
//     required this.valueX,
//     required this.valueY,
//   });

//   String key;
//   int no;
//   int valueX;
//   int valueY;

//   factory Data1.fromJson(Map<String, dynamic> json) => Data1(
//         key: json["key"],
//         no: json["no"],
//         valueX: json["value_x"],
//         valueY: json["value_y"],
//       );

//   Map<String, dynamic> toJson() => {
//         "key": key,
//         "no": no,
//         "value_x": valueX,
//         "value_y": valueY,
//       };
// }
