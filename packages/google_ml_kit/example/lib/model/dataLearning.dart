class dataLearning {
  int? id;
  String? key1;
  double? jarakX;
  double? jarakY;

  dataLearning({this.id, this.key1, this.jarakX, this.jarakY});

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    } else if (jarakX != null) {
      map['jarakX'] = jarakX;
    } else if (jarakY != null) {
      map['jarakY'] = jarakY;
    }
    return map;
  }

  dataLearning.fromMap(Map<String, dynamic> map) {
    this.id = map['id'];
    this.key1 = map['key1'];
    this.jarakX = map['jarakX'];
    this.jarakY = map['jarakY'];
  }
}
