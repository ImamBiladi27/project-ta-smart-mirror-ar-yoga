// // To parse this JSON data, do
// //
// //     final padahastana = padahastanaFromJson(jsonString);

// import 'dart:convert';

// List<Padahastana> padahastanaFromJson(String str) => List<Padahastana>.from(
//     json.decode(str).map((x) => Padahastana.fromJson(x)));

// String padahastanaToJson(List<Padahastana> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Padahastana {
//   Padahastana({
//     required this.key,
//     required this.no,
//     required this.valueX,
//     required this.valueY,
//     required this.akar,
//   });

//   String key;
//   int no;
//   double valueX;
//   double valueY;
//   double akar;
//   factory Padahastana.fromJson(Map<String, dynamic> json) => Padahastana(
//         key: json["key"],
//         no: json["no"],
//         valueX: json["value_x"]?.toDouble(),
//         valueY: json["value_y"]?.toDouble(),
//         akar: json["akar"]?.toDouble(),
//       );

//   Map<String, dynamic> toJson() => {
//         "key": key,
//         "no": no,
//         "value_x": valueX,
//         "value_y": valueY,
//         "akar": akar,
//       };
// }
// To parse this JSON data, do
//
//     final padahastana = padahastanaFromJson(jsonString);

//terbaru dari fajar
// import 'dart:convert';

// List<Padahastana> padahastanaFromJson(String str) => List<Padahastana>.from(
//     json.decode(str).map((x) => Padahastana.fromJson(x)));

// String padahastanaToJson(List<Padahastana> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Padahastana {
//   String key1;
//   String key2;
//   int jarak;

//   Padahastana({
//     required this.key1,
//     required this.key2,
//     required this.jarak,
//   });

//   factory Padahastana.fromJson(Map<String, dynamic> json) => Padahastana(
//         key1: json["key1"],
//         key2: json["key2"],
//         jarak: json["jarak"],
//       );

//   Map<String, dynamic> toJson() => {
//         "key1": key1,
//         "key2": key2,
//         "jarak": jarak,
//       };
// }

// To parse this JSON data, do
//
//     final padahastana = padahastanaFromJson(jsonString);

//terbaru 09-05-2023
// import 'dart:convert';

// List<Padahastana> padahastanaFromJson(String str) => List<Padahastana>.from(
//     json.decode(str).map((x) => Padahastana.fromJson(x)));

// String padahastanaToJson(List<Padahastana> data) =>
//     json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

// class Padahastana {
//   String key1;
//   String key2;
//   int jarak;
//   String key3;
//   String key4;
//   int jarak2;

//   Padahastana({
//     required this.key1,
//     required this.key2,
//     required this.jarak,
//     required this.key3,
//     required this.key4,
//     required this.jarak2,
//   });

//   factory Padahastana.fromJson(Map<String, dynamic> json) => Padahastana(
//         key1: json["key1"],
//         key2: json["key2"],
//         jarak: json["jarak"],
//         key3: json["key3"],
//         key4: json["key4"],
//         jarak2: json["jarak2"],
//       );

//   Map<String, dynamic> toJson() => {
//         "key1": key1,
//         "key2": key2,
//         "jarak": jarak,
//         "key3": key3,
//         "key4": key4,
//         "jarak2": jarak2,
//       };
// }

// To parse this JSON data, do
//
//     final padahastana = padahastanaFromJson(jsonString);

import 'dart:convert';

List<Padahastana> padahastanaFromJson(String str) => List<Padahastana>.from(
    json.decode(str).map((x) => Padahastana.fromJson(x)));

String padahastanaToJson(List<Padahastana> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Padahastana {
  String key1;
  String key2;
  int jarak;
  String key3;
  String key4;
  int jarak2;
  int batas;
  int batas2;
  int batas3;
  int batas4;
  int batas5;

  Padahastana({
    required this.key1,
    required this.key2,
    required this.jarak,
    required this.key3,
    required this.key4,
    required this.jarak2,
    required this.batas,
    required this.batas2,
    required this.batas3,
    required this.batas4,
    required this.batas5,
  });

  factory Padahastana.fromJson(Map<String, dynamic> json) => Padahastana(
        key1: json["key1"],
        key2: json["key2"],
        jarak: json["jarak"],
        key3: json["key3"],
        key4: json["key4"],
        jarak2: json["jarak2"],
        batas: json["batas"],
        batas2: json["batas2"],
        batas3: json["batas3"],
        batas4: json["batas4"],
        batas5: json["batas5"],
      );

  Map<String, dynamic> toJson() => {
        "key1": key1,
        "key2": key2,
        "jarak": jarak,
        "key3": key3,
        "key4": key4,
        "jarak2": jarak2,
        "batas": batas,
        "batas2": batas2,
        "batas3": batas3,
        "batas4": batas4,
        "batas5": batas5,
      };
}
