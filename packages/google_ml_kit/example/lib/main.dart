import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:google_ml_kit_example/vision_detector_views/DetailScreen2.dart';
import 'package:google_ml_kit_example/vision_detector_views/DetailScreen3.dart';
import 'package:google_ml_kit_example/vision_detector_views/DetailScreen4.dart';
import 'package:google_ml_kit_example/vision_detector_views/DetailScreen5.dart';
import 'package:google_ml_kit_example/vision_detector_views/DetailScreen6.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view2.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view3.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view4.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view5.dart';
import 'package:google_ml_kit_example/vision_detector_views/pose_detector_view6.dart';

// import 'nlp_detector_views/entity_extraction_view.dart';
// import 'nlp_detector_views/language_identifier_view.dart';
// import 'nlp_detector_views/language_translator_view.dart';
// import 'nlp_detector_views/smart_reply_view.dart';

import 'dart:async';

import 'vision_detector_views/DetailScreen1.dart';

List<CameraDescription> cameras = [];

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Smart Mirror AR'),
        centerTitle: true,
        elevation: 0,
      ),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: <Widget>[
                  ExpansionTile(
                    title: const Text('Press Touch'),
                    children: [
                      CustomCard('Padahastana', DetailsScreen()),
                      CustomCard('Virabhadrasana I', DetailsScreen2()),
                      CustomCard('Virabhadrasana II', DetailsScreen3()),
                      CustomCard('Virabhadrasana III', DetailsScreen4()),
                      CustomCard('Vrikshasana', DetailsScreen5()),
                      CustomCard('Bhujangasana', DetailsScreen6()),
                    ],
                  ),

                  // (_counter > 0)
                  //     ? Text("")
                  //     : Text(
                  //         "DONE!",
                  //         style: TextStyle(
                  //           color: Colors.green,
                  //           fontWeight: FontWeight.bold,
                  //           fontSize: 48,
                  //         ),
                  //       ),
                  // Text(
                  //   '$_counter',
                  //   style: TextStyle(
                  //     fontWeight: FontWeight.bold,
                  //     fontSize: 48,
                  //   ),
                  // ),

                  // ElevatedButton(
                  //   onPressed: () => _startTimer(),
                  //   child: Text("Start 10 second count down"),
                  // ),

                  SizedBox(
                    height: 20,
                  ),
                  // ExpansionTile(
                  //   title: const Text('Natural Language APIs'),
                  //   children: [
                  //     CustomCard('Language ID', LanguageIdentifierView()),
                  //     CustomCard(
                  //         'On-device Translation', LanguageTranslatorView()),
                  //     CustomCard('Smart Reply', SmartReplyView()),
                  //     CustomCard('Entity Extraction', EntityExtractionView()),
                  //   ],
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CustomCard extends StatelessWidget {
  final String _label;
  //final Image img;
  final Widget _viewPage;
  final bool featureCompleted;

  const CustomCard(this._label, this._viewPage, {this.featureCompleted = true});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.only(bottom: 10),
      child: ListTile(
        tileColor: Theme.of(context).primaryColor,
        title: Text(
          _label,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        onTap: () {
          if (!featureCompleted) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content:
                    const Text('This feature has not been implemented yet')));
          } else {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => _viewPage));
          }
        },
      ),
    );
  }
}
