import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../model/dataLearning.dart';
import '../model/dataTraining.dart';

class DbHelperTraining {
  static final DbHelperTraining _instance = DbHelperTraining._internal();
  static Database? _database;

  final String tableName = 'tableDataLearning';
  final String columnId = 'id';
  final String columnKey = 'key';
  final double columnJarakX = 'jarakX' as double;
  final double columnJarakY = 'jarakY' as double;
  DbHelperTraining._internal();
  factory DbHelperTraining() => _instance;

  Future<Database?> get _db async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDb();
    return _database;
  }

  Future<Database?> _initDb() async {
    String databasePath = await getDatabasesPath();
    String path = join(databasePath, 'dataLearning.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future<void> _onCreate(Database db, int version) async {
    var sql = "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY, "
        "$columnKey TEXT,"
        "$columnJarakX DOUBLE,"
        "$columnJarakY DOUBLE,";
    await db.execute(sql);
  }

  Future<int?> saveDataLearning(dataLearning dataLearning) async {
    var dbClient = await _db;
    return await dbClient!.insert(tableName, dataLearning.toMap());
  }

  Future<List?> getAllDataLearning() async {
    var dbClient = await _db;
    var result = await dbClient!.query(tableName, columns: [
      columnId,
      columnKey,
      columnJarakX.toString(),
      columnJarakY.toString(),
    ]);
    return result.toList();
  }

  // Future<int?> updateDataTraining(dataTraining datatraining) async {
  //   var dbClient = await _db;
  //   return await dbClient!.update(tableName, datatraining.toMap(),
  //       where: '$columnKey=?', whereArgs: [datatraining.id]);
  // }

  // Future<int?> deleteDataTraining(int id) async {
  //   var dbClient = await _db;
  //   return await dbClient!
  //       .delete(tableName, where: '$columnKey=?', whereArgs: [id]);
  // }
}
