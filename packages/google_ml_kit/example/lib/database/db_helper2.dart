import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../model/dataTraining.dart';

class DbHelper2 {
  static final DbHelper2 _instance = DbHelper2._internal();
  static Database? _database;

  final String tableName = 'tableDataTraining';
  final String columnId = 'id';
  final String columnKey = 'key';
  final String columnKey2 = 'key2';
  final int columnJarak = 'jarak' as int;
  final String columnKey3 = 'key3';
  final String columnKey4 = 'key4';
  final int columnJarak2 = 'jarak2' as int;
  final int columnbatas = 'batas' as int;
  final int columnbatas2 = 'batas2' as int;
  final int columnbatas3 = 'batas3' as int;
  final int columnbatas4 = 'batas4' as int;
  final int columnbatas5 = 'batas5' as int;

  DbHelper2._internal();
  factory DbHelper2() => _instance;

  Future<Database?> get _db async {
    if (_database != null) {
      return _database;
    }
    _database = await _initDb();
    return _database;
  }

  Future<Database?> _initDb() async {
    String databasePath = await getDatabasesPath();
    String path = join(databasePath, 'dataCorrection.db');
    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future<void> _onCreate(Database db, int version) async {
    var sql = "CREATE TABLE $tableName($columnId INTEGER PRIMARY KEY, "
        "$columnKey TEXT,"
        "$columnKey2 TEXT,"
        "$columnJarak INTEGER,"
        "$columnKey3 TEXT,"
        "$columnKey4 TEXT,"
        "$columnJarak2 INTEGER,"
        "$columnbatas INTEGER,"
        "$columnbatas2 INTEGER,"
        "$columnbatas3 INTEGER,"
        "$columnbatas4 INTEGER,"
        "$columnbatas5)";
    await db.execute(sql);
  }

  Future<int?> saveDataTraining(dataTraining datatraining) async {
    var dbClient = await _db;
    return await dbClient!.insert(tableName, datatraining.toMap());
  }

  Future<List?> getAllDataTraining() async {
    var dbClient = await _db;
    var result = await dbClient!.query(tableName, columns: [
      columnId,
      columnKey,
      columnKey2,
      columnJarak.toString(),
      columnKey3,
      columnKey4,
      columnJarak2.toString(),
      columnbatas.toString(),
      columnbatas2.toString(),
      columnbatas3.toString(),
      columnbatas4.toString(),
      columnbatas5.toString(),
    ]);
    return result.toList();
  }

  Future<int?> updateDataTraining(dataTraining datatraining) async {
    var dbClient = await _db;
    return await dbClient!.update(tableName, datatraining.toMap(),
        where: '$columnbatas=?', whereArgs: [datatraining.id]);
  }

  Future<int?> deleteDataTraining(int id) async {
    var dbClient = await _db;
    return await dbClient!
        .delete(tableName, where: '$columnbatas=?', whereArgs: [id]);
  }
}
